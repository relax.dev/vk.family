import {
  Button,
  Header,
  HorizontalCell,
  HorizontalScroll,
} from '@vkontakte/vkui';
import { openFamilyAddPage } from '../../../modules/router/moves';
import { useUser } from '../../../modules/user/useUser';
import { FamilyListMember } from './FamilyListMember';

export interface FamilyListProps {
  userIds: number[];
  extended?: boolean;
}

export const FamilyList = (props: FamilyListProps) => {
  const { user } = useUser();

  const { userIds, extended } = props;

  return (
    <>
      <Header
        mode="secondary"
        aside={
          extended && (
            <Button mode="tertiary" onClick={openFamilyAddPage}>
              Пригласить
            </Button>
          )
        }>
        Моя семья
      </Header>
      <HorizontalScroll>
        <div style={{ display: 'flex' }}>
          {userIds.map((id) => {
            return (
              <FamilyListMember isCurrent={id === user?.id} key={id} id={id} />
            );
          })}
        </div>
      </HorizontalScroll>
    </>
  );
};
