import { BridgePlus } from '@happysanta/bridge-plus';
import {
  Avatar,
  Caption,
  HorizontalCell,
  Spacing,
  Spinner,
} from '@vkontakte/vkui';
import { memo, useEffect, useState } from 'react';
import { addToCache, getFromCache } from '../../..';

export interface FamilyListMemberProps {
  id: number;
  isCurrent: boolean;
  onClick?: () => void;
}

export const useMember = (id: number) => {
  const [user, setUser] = useState<any>(getFromCache(id));
  const [error, setError] = useState(false);

  const handleFetchUser = async (id: number) => {
    const user = await getFromCache(id);

    if (user) {
      setUser(user);
    } else {
      addToCache(
        id,
        BridgePlus.api('users.get', {
          user_ids: id,
          fields: 'photo_200',
        })
          .then(({ response }) => {
            setUser(response[0]);
            addToCache(id, response[0]);
          })
          .catch(() => {
            setError(true);
          }),
      );
    }
  };

  useEffect(() => {
    handleFetchUser(id);
  }, [id]);

  return {
    error,
    user,
  };
};
export const FamilyListMember = (props: FamilyListMemberProps) => {
  const { error, user } = useMember(props.id);

  if (error) {
    return null;
  }

  if (!user) {
    return (
      <div>
        <Spinner style={{ width: 64, height: 80 }} />
      </div>
    );
  }

  return (
    <HorizontalCell
      header={props.isCurrent ? 'Вы' : user.first_name}
      href={!props.onClick && 'https://vk.com/id' + user.id}
      target="_blank"
      onClick={props.onClick}>
      <Avatar size={48} src={user.photo_200} />
    </HorizontalCell>
  );
};
