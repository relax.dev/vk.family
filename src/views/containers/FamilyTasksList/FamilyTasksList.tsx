import {
  Icon16ClockCircleFill,
  Icon16ClockOutline,
  Icon24Add,
  Icon24ClockOutline,
  Icon36ClockOutline,
} from '@vkontakte/icons';
import {
  Avatar,
  Button,
  Caption,
  CellButton,
  Div,
  List,
  SimpleCell,
  Spacing,
  Spinner,
  Title,
} from '@vkontakte/vkui';
import moment from 'moment';
import { useFamilyTasks } from '../../../modules/api';
import {
  openFamilyTask,
  openFamilyTaskAdd,
} from '../../../modules/router/moves';
import { useUser } from '../../../modules/user/useUser';
import { time } from '../../../utils/time';
import { MembersStack } from '../MembersStack/MembersStack';

export interface FamilyTasksListProps {
  day: Date;
  isToday: boolean;
  isTomorrow: boolean;
  isFlat?: boolean;
}

function capitalizeFirstLetter(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export const FamilyTasksList = (props: FamilyTasksListProps) => {
  const { user } = useUser();
  const { day, isToday, isFlat, isTomorrow } = props;

  const { data, isLoading } = useFamilyTasks({
    date: time(day).format('YYYY-MM-DD'),
  });

  if (!data?.data?.data?.length && isFlat) return null;

  console.log('asdasdsa', data?.data);
  return (
    <div style={{ paddingBottom: 48 }}>
      <Div style={{ display: 'flex', alignItems: 'center' }}>
        <Title level="1">
          {isToday
            ? 'Сегодня'
            : isTomorrow
            ? 'Завтра'
            : time(day).format('DD MMMM')}
        </Title>
      </Div>
      {data?.data?.data?.length ? (
        <>
          <List>
            {data.data.data.map((task) => {
              return (
                <SimpleCell
                  key={task.id}
                  onClick={() => openFamilyTask(task.id)}
                  before={<Icon24ClockOutline />}
                  after={<MembersStack ids={task.members.map((id) => +id)} />}
                  description={`${time(Number(task.date_start) * 1000).format(
                    'HH:mm dd',
                  )} -
                  ${time(Number(task.date_finish) * 1000).format('HH:mm dd')}`}>
                  {task.name}
                </SimpleCell>
              );
            })}
          </List>
          {!isFlat && (
            <CellButton
              centered
              before={<Icon24Add />}
              onClick={openFamilyTaskAdd}>
              Запланировать
            </CellButton>
          )}
        </>
      ) : (
        <div
          style={{
            width: '100%',
            height: '100%',
            minHeight: 150,
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column',
          }}>
          {isLoading ? (
            <Spinner />
          ) : (
            <>
              <Title level="2">На этот день нет планов 😞</Title>
              {!isFlat && (
                <>
                  <Spacing size={12} />
                  <Button size="l" onClick={openFamilyTaskAdd}>
                    Запланировать дело
                  </Button>
                </>
              )}
            </>
          )}
        </div>
      )}
    </div>
  );
};
