import { UsersStack } from '@vkontakte/vkui';
import { UseUsersData } from '../../screens/FamilyTaskAdd/FamilyTaskAdd';

export const MembersStack = ({ ids }: { ids: number[] }) => {
  const members = UseUsersData(ids);

  return <UsersStack photos={members.map(({ photo_200 }) => photo_200)} />;
};
