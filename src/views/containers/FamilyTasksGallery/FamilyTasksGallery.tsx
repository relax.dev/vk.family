import { Gallery } from '@vkontakte/vkui';
import moment from 'moment';
import { getDaysArray } from '../../../utils/time';
import { FamilyTasksList } from '../FamilyTasksList/FamilyTasksList';

export const FamilyTasksGallery = () => {
  const days = getDaysArray();

  return (
    <Gallery slideWidth="100%" bullets="dark">
      {days.map((day) => {
        return (
          <FamilyTasksList
            day={day}
            isToday={moment().isSame(moment(day), 'day')}
            isTomorrow={moment().add(1, 'day').isSame(moment(day), 'day')}
          />
        );
      })}
    </Gallery>
  );
};
