import { useFirstPageCheck, useRouter } from '@happysanta/router';
import {
  PanelHeader,
  PanelHeaderBack,
  PanelHeaderButton,
  PanelHeaderProps,
} from '@vkontakte/vkui';
import { PAGE_MAIN } from '../../../modules/router';

export const PanelHeaderBase = ({ children, ...props }: PanelHeaderProps) => {
  const router = useRouter();
  const isFirstPage = useFirstPageCheck();

  return (
    <PanelHeader
      left={
        <PanelHeaderBack
          onClick={() => {
            isFirstPage ? router.replacePage(PAGE_MAIN) : router.popPage();
          }}
        />
      }
      {...props}>
      {children}
    </PanelHeader>
  );
};
