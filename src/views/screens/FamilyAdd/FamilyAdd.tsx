import { BridgePlus } from '@happysanta/bridge-plus';
import {
  Avatar,
  Button,
  Cell,
  Div,
  FixedLayout,
  FormField,
  FormItem,
  FormLayout,
  Header,
  Input,
  List,
  Panel,
  PanelHeader,
  PanelHeaderEdit,
  PanelHeaderSubmit,
  PanelProps,
  Group,
  Text,
} from '@vkontakte/vkui';
import { ChangeEvent, useEffect, useState } from 'react';
import { addToCache, getFromCache } from '../../..';
import { useFamily, useFamilyAddMember } from '../../../modules/api';
import { openMainPage } from '../../../modules/router/moves';
import { useUser } from '../../../modules/user/useUser';
import { PanelHeaderBase } from '../../components/PanelHeaderBase/PanelHeaderBase';
import { FamilyList } from '../../containers/FamilyList/FamilyList';

export const FamilyAdd = (props: PanelProps) => {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState<any>(null);
  const [error, setError] = useState<boolean>(false);
  const [url, setUrl] = useState('');
  const [loading, setLoading] = useState(false);

  const add = useFamilyAddMember();
  const { data: family } = useFamily();

  const handleFetchUser = async (url: string) => {
    const id = url
      .replaceAll('https://vk.com/', '')
      .replaceAll('https://vk.ru/', '');

    const result = await getFromCache(id);

    if (result) {
      setUser(result);
    } else {
      setLoading(true);

      addToCache(
        id,
        BridgePlus.api('users.get', {
          user_ids: id,
          fields: 'photo_200',
        })
          .then(({ response }) => {
            setUser(response[0]);
            addToCache(id, response[0]);
          })
          .catch(() => {
            setError(true);
          })
          .finally(() => {
            setLoading(false);
          }),
      );
    }
  };

  const handleAddUser = (ids: number[]) => {
    setError(false);
    add
      .mutateAsync({ user_ids: ids })
      .then((response) => {
        console.log('add', response);
        if (response.data.success) {
          setUser(false);
        } else {
          throw new Error('Ошибка');
        }
      })
      .catch(() => {
        setError(true);
      });
    // setFamily([...family, ...ids]);
  };

  const handleLoadFriends = () => {
    BridgePlus.getFriends(true).then(({ users }) => {
      handleAddUser(users?.map(({ id }) => id) || []);
    });
  };

  const handleChangeSearch = (e: ChangeEvent<HTMLInputElement>) => {
    setUrl(e.target.value);
  };

  return (
    <Panel {...props}>
      <PanelHeaderBase>Приглашение</PanelHeaderBase>
      <Group>
        {!!family?.data.user_ids.length && (
          <FamilyList userIds={family?.data.user_ids} />
        )}
        <Header mode="secondary">По ссылке</Header>
        <FormLayout>
          <FormItem top="Введите ссылку на профиль пользователя">
            <Input
              placeholder="https://vk.ru/web.rnskv"
              value={url}
              onChange={handleChangeSearch}
              after={
                !!url.length && (
                  <Button
                    loading={loading}
                    mode="tertiary"
                    onClick={() => handleFetchUser(url)}>
                    Готово
                  </Button>
                )
              }
            />
          </FormItem>
        </FormLayout>
        {user && (
          <Cell
            before={<Avatar src={user.photo_200} />}
            after={
              <Button
                mode="tertiary"
                size="l"
                loading={add.isLoading}
                onClick={() => handleAddUser([user.id])}>
                Пригласить
              </Button>
            }>
            {user.first_name} {user.last_name}
          </Cell>
        )}
        {error && (
          <Div>
            <Text weight="regular" style={{ color: 'var(--destructive)' }}>
              Возникла ошибка
            </Text>
          </Div>
        )}
        <Header mode="secondary">Из списка друзей</Header>
        <Div>
          <Button
            size="l"
            mode="secondary"
            onClick={handleLoadFriends}
            stretched>
            Открыть список
          </Button>
        </Div>

        <FixedLayout vertical="bottom">
          <Div>
            <Button size="l" stretched onClick={openMainPage}>
              Завершить
            </Button>
          </Div>
        </FixedLayout>
      </Group>
    </Panel>
  );
};
