import { useParams } from '@happysanta/router';
import { Button, ModalCard, ModalCardProps } from '@vkontakte/vkui';
import {
  useFamilyAddMember,
  useFamilyTask,
  useFamilyTaskAdd,
  useFamilyTaskAddMember,
  useFamilyTaskRemove,
} from '../../../modules/api';
import { useUser } from '../../../modules/user/useUser';
import { time } from '../../../utils/time';
import { MembersStack } from '../../containers/MembersStack/MembersStack';

export const FamilyTask = (props: ModalCardProps) => {
  const taskId = useParams().taskId;
  const task = useFamilyTask(+taskId);
  const { user } = useUser();
  const remove = useFamilyTaskRemove();
  const join = useFamilyTaskAddMember();

  return (
    <ModalCard
      {...props}
      header={task.data.name}
      subheader={`${time(Number(task.data.date_start) * 1000).format(
        'HH:mm dd',
      )} -
                  ${time(Number(task.data.date_finish) * 1000).format(
                    'HH:mm dd',
                  )}`}
      actions={
        !task.data.members.includes(String(user.id)) && [
          <Button
            size="l"
            loading={join.isLoading}
            onClick={() => {
              join
                .mutateAsync({ user_ids: [user.id], id: +taskId })
                .then(() => {
                  props.onClose();
                });
            }}>
            Присоедениться
          </Button>,
        ]
      }>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
        }}>
        <MembersStack ids={task.data.members} />
        <Button
          mode="destructive"
          size="s"
          loading={remove.isLoading}
          onClick={() => {
            remove.mutateAsync(task.data.id).then(() => {
              props.onClose();
            });
          }}>
          Удалить задачу
        </Button>
      </div>
    </ModalCard>
  );
};
