import { Panel, PanelProps, Group } from '@vkontakte/vkui';
import { getDaysArray, time } from '../../../utils/time';
import { PanelHeaderBase } from '../../components/PanelHeaderBase/PanelHeaderBase';
import { FamilyTasksList } from '../../containers/FamilyTasksList/FamilyTasksList';

export const FamilyTasks = (props: PanelProps) => {
  const days = getDaysArray();

  return (
    <Panel {...props}>
      <PanelHeaderBase>Семейные планы</PanelHeaderBase>
      <Group>
        {days.map((day) => (
          <FamilyTasksList
            day={day}
            isFlat
            isToday={time().isSame(time(day), 'day')}
            isTomorrow={time().add(1, 'day').isSame(time(day), 'day')}
          />
        ))}
      </Group>
    </Panel>
  );
};
