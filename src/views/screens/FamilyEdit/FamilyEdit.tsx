import { Icon28HeartCircleOutline } from '@vkontakte/icons';
import {
  Avatar,
  Button,
  Caption,
  Div,
  FormItem,
  FormLayout,
  Header,
  Headline,
  Panel,
  PanelHeader,
  PanelProps,
  Spacing,
  Subhead,
  Text,
  Title,
} from '@vkontakte/vkui';
import './FamilyEdit.scss';
import family1 from '../../../assets/family_1.png';
import { useUser } from '../../../modules/user/useUser';
import { openFamilyAddPage, openMainPage } from '../../../modules/router/moves';

export const FamilyEdit = (props: PanelProps) => {
  const { geoData } = useUser();

  const handleEditFamily = () => {
    openFamilyAddPage();
  };

  return (
    <Panel {...props}>
      <div className="FamilyEdit">
        <Div className="FamilyEdit__content">
          <Title level="1" weight="semibold" style={{ textAlign: 'center' }}>
            Отлично!
            <br />
            Пространство создано!
          </Title>
          <Spacing size={12} />
          <Caption style={{ textAlign: 'center' }}>
            Теперь пригласи своих родных.
          </Caption>
        </Div>
        <Div className="FamilyEdit__content">
          <img src={family1} width={256} />
          <Spacing />
          <Button
            before={<Icon28HeartCircleOutline />}
            size={'l'}
            onClick={handleEditFamily}>
            Пригласить
          </Button>
        </Div>
      </div>
    </Panel>
  );
};
