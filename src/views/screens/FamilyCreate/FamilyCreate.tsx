import { Icon28HeartCircleOutline } from '@vkontakte/icons';
import {
  Avatar,
  Button,
  Caption,
  Div,
  FormItem,
  FormLayout,
  Header,
  Headline,
  Panel,
  PanelHeader,
  PanelProps,
  Spacing,
  Subhead,
  Text,
  Title,
} from '@vkontakte/vkui';
import './FamilyCreate.scss';
import family1 from '../../../assets/family_1.png';
import { useUser } from '../../../modules/user/useUser';
import {
  openFamilyEditPage,
  openMainPage,
} from '../../../modules/router/moves';
import { useFamilyCreate } from '../../../modules/api';

export const FamilyCreate = (props: PanelProps) => {
  const { user } = useUser();

  const creation = useFamilyCreate();

  const handleCreateFamily = () => {
    creation.mutateAsync().then((response) => {
      openFamilyEditPage();
    });
  };

  return (
    <Panel {...props}>
      <div className="FamilyCreate">
        <Div className="FamilyCreate__content">
          <Title level="1" weight="semibold">
            Привет, {user?.first_name} 👋
          </Title>
          <Spacing size={12} />
          <Caption style={{ textAlign: 'center' }}>
            Создай новое семейное пространство <br />
            или
            <br />
            получи приглашение от одного из <br />
            членов семьи в существующее.
          </Caption>
        </Div>
        <Div className="FamilyCreate__content">
          <img src={family1} width={256} />
          <Spacing />
          <Button
            loading={creation.isLoading}
            before={<Icon28HeartCircleOutline />}
            size={'l'}
            disabled={creation.isLoading}
            onClick={handleCreateFamily}>
            Создать
          </Button>
        </Div>
      </div>
    </Panel>
  );
};
