import { BridgePlus } from '@happysanta/bridge-plus';
import {
  Avatar,
  Button,
  CustomSelect,
  CustomSelectOption,
  DateInput,
  DateRangeInput,
  Div,
  FixedLayout,
  FormItem,
  FormLayout,
  Input,
  ModalDismissButton,
  ModalPage,
  ModalPageHeader,
  ModalPageProps,
  PanelHeaderButton,
  Select,
  SimpleCell,
  Spinner,
} from '@vkontakte/vkui';
import { Form, useFormik } from 'formik';
import moment from 'moment';
import { memo, useEffect } from 'react';
import { useMemo } from 'react';
import { useState } from 'react';
import { useFamily, useFamilyTaskAdd } from '../../../modules/api';
import { useMember } from '../../containers/FamilyList/FamilyListMember';
import { ChipsSelect } from '@vkontakte/vkui/dist/unstable';
import { time } from '../../../utils/time';
import { addToCacheMany, getFromCacheMany } from '../../..';
import { router } from '../../../modules/router';
import { Icon24Dismiss } from '@vkontakte/icons';

export const UserOption = (props: any) => {
  console.log('props', props);

  return (
    <CustomSelectOption
      before={<Avatar size={32} src={props.option.photo_200} />}
      {...props}>
      {props.option.first_name} {props.option.last_name}
    </CustomSelectOption>
  );
};

export const UseUsersData = (ids: number[]) => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getFromCacheMany(ids).then((result) => {
      if (result) {
        setUsers(result as any);
      } else {
        addToCacheMany(
          ids,
          BridgePlus.api('users.get', {
            user_ids: ids.join(','),
            fields: 'photo_200',
          }).then((response) => {
            console.log(response);
            addToCacheMany(ids, response.response);
            setUsers(response.response as any);
          }),
        );
      }
    });
  }, [ids.join(',')]);

  return users || [];
};

export const FamilyTaskAdd = (props: ModalPageProps) => {
  const { data: family } = useFamily();

  const users = UseUsersData(family?.data?.user_ids || []);

  const add = useFamilyTaskAdd();

  const options =
    (users?.length ? users : []).map((user: any) => {
      return {
        ...user,
        label: user.first_name + ' ' + user.last_name,
        value: user.id,
      };
    }) || [];

  const formik = useFormik({
    initialValues: {
      text: '',
      participants: [],
      startDate: undefined,
      endDate: undefined,
    },
    onSubmit: (values) => {
      const ids = values.participants.map(({ id }) => id);

      add
        .mutateAsync({
          name: values.text,
          user_ids: ids,
          date_start: time(values.startDate).unix(),
          date_finish: values.endDate ? time(values.endDate).unix() : undefined,
        })
        .then((response) => {
          router.popPage();
        });
    },
  });

  return (
    <ModalPage {...props}>
      <form onSubmit={formik.handleSubmit}>
        <ModalPageHeader
          right={
            <PanelHeaderButton onClick={props.onClose}>
              <Icon24Dismiss />
            </PanelHeaderButton>
          }>
          Запланировать дело
        </ModalPageHeader>
        <Div>
          <FormLayout>
            <FormItem top={'Участники*'} required>
              <ChipsSelect
                required
                name="owner"
                creatable={false}
                value={formik.values.participants}
                options={options}
                renderOption={(props) => (
                  <UserOption key={props.id} {...props} />
                )}
                onChange={(value) =>
                  formik.setFieldValue('participants', value)
                }
              />
            </FormItem>
            <FormItem top={'Что нужно сделать?*'} required>
              <Input
                required
                placeholder="Вынести мусор"
                name="text"
                onChange={formik.handleChange}
              />
            </FormItem>
            <FormItem top={'Когда нужно начать?*'} required>
              <DateInput
                required
                enableTime={true}
                disablePast
                value={formik.values.startDate}
                onChange={(value) => formik.setFieldValue('startDate', value)}
              />
            </FormItem>
            <FormItem top={'Когда нужно закончить?'}>
              <DateInput
                enableTime={true}
                value={formik.values.endDate}
                onChange={(value) => formik.setFieldValue('endDate', value)}
              />
            </FormItem>
          </FormLayout>
        </Div>
        <Div>
          <Button
            disabled={
              !(
                formik.values.startDate &&
                formik.values.participants.length &&
                formik.values.text.length
              )
            }
            size="l"
            stretched
            type="submit"
            loading={add.isLoading}>
            Запланировать
          </Button>
        </Div>
      </form>
    </ModalPage>
  );
};
