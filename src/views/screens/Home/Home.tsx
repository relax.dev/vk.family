import {
  Icon56ArticleOutline,
  Icon56CalendarOutline,
  Icon56DocumentOutline,
  Icon56ListPlayOutline,
  Icon56LockOutline,
  Icon56PlaceOutline,
  Icon56TabletOutline,
  Icon56WalletOutline,
} from '@vkontakte/icons';
import {
  Avatar,
  Button,
  Caption,
  Card,
  Div,
  Group,
  Header,
  Panel,
  PanelHeader,
  PanelProps,
  Spacing,
  useAdaptivity,
  Title,
} from '@vkontakte/vkui';
import { url } from 'inspector';
import { userInfo } from 'os';
import { useUser } from '../../../modules/user/useUser';
import { FamilyTasksList } from '../../containers/FamilyTasksList/FamilyTasksList';
import world from '../../../assets/world.png';

import './Home.scss';
import { useFamily, useFamilyGeoSend } from '../../../modules/api';
import { FamilyListMember } from '../../containers/FamilyList/FamilyListMember';
import { FamilyList } from '../../containers/FamilyList/FamilyList';
import {
  openFamilyGeoPage,
  openFamilyTaskAdd,
  openFamilyTasks,
} from '../../../modules/router/moves';
import { FamilyTasksGallery } from '../../containers/FamilyTasksGallery/FamilyTasksGallery';
import { useState } from 'react';
import { useEffect } from 'react';
import { BridgePlus } from '@happysanta/bridge-plus';

export const MenuItem = (props: {
  title: string;
  icon: any;
  background?: string;
  onClick?: () => void;
  backgroundColor?: string;
  disabled?: boolean;
}) => {
  return (
    <Card
      className={'Home__card'}
      onClick={props.onClick}
      style={
        props.disabled
          ? {
              opacity: 0.7,
              pointerEvents: 'none',
            }
          : {
              background:
                'linear-gradient(45deg, rgb(161 191 204 / 12%), rgb(112 207 255 / 36%))',
            }
      }>
      {props.disabled && (
        <div
          style={{
            display: 'flex',
            position: 'absolute',
            top: -6,
            right: -8,
            justifyContent: 'center',
            alignItems: 'center',
            background: 'var(--accent)',
            color: 'var(--white)',
            borderRadius: 8,
            padding: '4px 6px',
            fontSize: 12,
            pointerEvents: 'none',
          }}>
          Скоро...
        </div>
      )}
      <Div
        style={{
          background: `url(${props.background})`,
          backgroundSize: 'cover',
          backgroundPosition: '0 0',
          overflow: 'hidden',
          borderRadius: 8,
          backgroundColor: props.backgroundColor,
        }}>
        {props.icon}
        <Spacing size={4} />
        <Title level="3">{props.title}</Title>
      </Div>
    </Card>
  );
};

export const Home = (props: PanelProps) => {
  const { user } = useUser();
  const { data: family } = useFamily();

  const send = useFamilyGeoSend();

  const [counter, setCounter] = useState(0);

  const adaptivity = useAdaptivity();

  useEffect(() => {
    let id: any;
    BridgePlus.getGeodata().then((data: any) => {
      if (data.lat && data.long) {
        send.mutateAsync({ lat: data.lat, lng: data.long }).finally(() => {
          id = setTimeout(() => {
            setCounter(counter + 1);
          }, 5000);
        });
      }
    });

    return () => clearTimeout(id);
  }, [counter]);

  if (!family?.data?.user_ids?.length) {
    return null;
  }

  return (
    <Panel {...props}>
      <Group>
        <Div
          style={{
            display: 'flex',
            alignItems: 'center',
            margin: '56px 0px 0',
          }}>
          <Avatar size={64} src={user?.photo_200} />
          <Title level={'1'} style={{ marginLeft: 12 }}>
            {user?.first_name} ✨
          </Title>
        </Div>

        <Header mode="secondary">Cемейное пространство</Header>

        <div className="Home__container">
          <div className="Home__container-side">
            <MenuItem
              title="Календарь"
              disabled
              icon={
                <Icon56CalendarOutline style={{ color: 'var(--accent)' }} />
              }
            />
            <MenuItem
              title="Документы"
              disabled
              icon={
                <Icon56DocumentOutline style={{ color: 'var(--accent)' }} />
              }
            />
          </div>
          <div className="Home__container-side">
            <MenuItem
              title="Планы"
              onClick={openFamilyTasks}
              icon={<Icon56ArticleOutline style={{ color: 'var(--accent)' }} />}
            />
            <MenuItem
              title="Скидки"
              disabled
              icon={<Icon56WalletOutline style={{ color: 'var(--accent)' }} />}
            />
          </div>

          <div className="Home__container-side--full">
            <MenuItem
              title="Карта"
              onClick={openFamilyGeoPage}
              background={world}
              icon={<Icon56PlaceOutline style={{ color: 'var(--accent)' }} />}
            />
          </div>
        </div>

        <Spacing />

        {family?.data.user_ids.length && (
          <FamilyList userIds={family?.data.user_ids} extended />
        )}

        <Spacing />
        <FamilyTasksGallery />
        <Spacing size={24} />
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            color: 'var(--text_secondary)',
            textAlign: 'center',
          }}>
          <Caption>
            Made with crutchs
            <br />
            Spring code &#10084;
          </Caption>
        </div>

        <Spacing size={24} />
      </Group>
    </Panel>
  );
};
