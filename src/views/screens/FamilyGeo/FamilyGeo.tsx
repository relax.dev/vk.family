import {
  Avatar,
  FixedLayout,
  HorizontalScroll,
  Panel,
  PanelProps,
  Spinner,
  Div,
  Header,
  Group,
} from '@vkontakte/vkui';
import { useUser } from '../../../modules/user/useUser';
import { PanelHeaderBase } from '../../components/PanelHeaderBase/PanelHeaderBase';
import { useFamilyGeo, useFamilyGeoSend } from '../../../modules/api';
import {
  FamilyListMember,
  useMember,
} from '../../containers/FamilyList/FamilyListMember';
import { Map, Marker, MarkerLayout } from 'yandex-map-react';
import { memo, useEffect } from 'react';
import { FamilyList } from '../../containers/FamilyList/FamilyList';
import { useState } from 'react';
import { BridgePlus } from '@happysanta/bridge-plus';

const MarkerCustom = memo(({ id, lat, lng }: any) => {
  const { user, error } = useMember(+id);

  if (!user) return null;

  return (
    <Marker lat={lat} lon={lng}>
      <MarkerLayout>
        <div
          style={{
            borderRadius: '50%',
            overflow: 'hidden',
            border: '3px solid white',
            width: 48,
            height: 48,
          }}>
          <img src={user.photo_200} width={48} style={{ objectFit: 'cover' }} />
        </div>
      </MarkerLayout>
    </Marker>
  );
});

export const FamilyGeo = memo(
  (props: PanelProps) => {
    const { geoData, user } = useUser();

    const { data: geos } = useFamilyGeo();

    const list = geos?.data.data || [];

    const [center, setCenter] = useState([]);
    const [zoom, setZoom] = useState(13);

    const send = useFamilyGeoSend();

    const [counter, setCounter] = useState(0);

    useEffect(() => {
      let id: any;
      BridgePlus.getGeodata().then((data: any) => {
        if (data.lat && data.long) {
          send.mutateAsync({ lat: data.lat, lng: data.long }).finally(() => {
            id = setTimeout(() => {
              setCounter(counter + 1);
            }, 5000);
          });
        }
      });

      return () => clearTimeout(id);
    }, [counter]);

    useEffect(() => {
      if (!center.length && geoData.lat && geoData.long) {
        setCenter([geoData.lat, geoData.long]);
        setZoom(18);
      }
    }, [geoData]);

    return (
      <Panel {...props}>
        <FixedLayout filled>
          <PanelHeaderBase>Семейная карта</PanelHeaderBase>
        </FixedLayout>
        <FixedLayout filled vertical="bottom">
          <Div>
            <HorizontalScroll>
              <div style={{ display: 'flex' }}>
                {list
                  .filter(({ lat, lng }) => lat !== null && lng !== null)
                  .map(({ id, lat, lng }) => {
                    return (
                      <FamilyListMember
                        isCurrent={+id === user?.id}
                        key={+id}
                        id={+id}
                        onClick={() => setCenter([lat, lng])}
                      />
                    );
                  })}
              </div>
            </HorizontalScroll>
          </Div>
        </FixedLayout>
        <Group>
          <div
            style={{
              height: '100vh',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Map width="100%" height="100%" center={center} zoom={zoom}>
              {list
                .filter(({ lat, lng }) => lat !== null && lng !== null)
                .map(({ lat, lng, id }: any) => (
                  <MarkerCustom key={id} id={id} lat={+lat} lng={+lng} />
                ))}
            </Map>
          </div>
        </Group>
      </Panel>
    );
  },
  (props, nProps) => props.id === nProps.id,
);
