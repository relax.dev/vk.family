export class UsersCache {
    private static instance: UsersCache;
    public cache = {};
    private constructor() {
        this.cache = {}
    }
    public static getInstance(): UsersCache {
        if (!UsersCache.instance) {
            UsersCache.instance = new UsersCache();
        }

        return UsersCache.instance;
    }
}

export let tasksCache: any = {};
