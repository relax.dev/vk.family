import { BridgePlus } from '@happysanta/bridge-plus';
import axios from 'axios';
import { QueryClient, useMutation, useQuery } from 'react-query';

export const API_URL = 'https://vkfamily.delo-vkusa.net/api/';

axios.interceptors.request.use((config) => {
  return {
    ...config,
    params: {
      ...config.params,
      ...BridgePlus.getStartParams().params,
    },
  };
});

axios.defaults.baseURL = API_URL;

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 60 * 1000,
    },
  },
});

export interface FamilyCreateResponse {
  uuid: string;
  success: boolean;
  msg: string;
}
export const useFamilyCreate = () =>
  useMutation(
    () => {
      return axios.post<FamilyCreateResponse>('family/create');
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('family');
      },
    },
  );

export interface FamilyAddMemberParams {
  user_ids: number[];
  id?: number;
}
export interface FamilyAddMemberResponse {
  uuid: string;
  data: string[];
  success: boolean;
}

export const useFamilyAddMember = () =>
  useMutation(
    (body: FamilyAddMemberParams) => {
      return axios.post<FamilyAddMemberResponse>('family/addMember', body);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('family');
      },
    },
  );

export interface FamilyGetResponse {
  uuid: string;
  user_ids: number[];
  success: boolean;
}
export const useFamily = () =>
  useQuery(['family'], () => {
    return axios.get<FamilyGetResponse>('family');
  });

export interface FamilyGeoSendParams {
  lat: number;
  lng: number;
}

export interface FamilyGeoSendResponse {
  success: boolean;
}

export interface FamilyGeoGetResponse {
  data: Array<{ id: string; lat: number; lng: number }>;
}

export const useFamilyGeoSend = () =>
  useMutation(
    (body: FamilyGeoSendParams) => {
      return axios.post<FamilyGeoSendResponse>('geo', body);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('geo');
      },
    },
  );

export const useFamilyGeo = () =>
  useQuery(['geo'], () => {
    return axios.get<FamilyGeoGetResponse>('geo');
  });

export interface IFamilyTask {
  id: string;
  family_id: string;
  name: string;
  date_start: string;
  date_finish: string;
  location?: string;
  description?: string;
  members: string[];
}

export interface FamilyGetCalendarParams {
  date: string;
}
export interface FamilyGetCalendarResponse {
  success: boolean;
  data: IFamilyTask[];
}

export interface FamilyCalendarAddParams {
  name: string;
  date_start: number;
  date_finish: number;
  location?: string;
  user_ids: number[];
}

export interface FamilyCalendarAddResponse {
  data: IFamilyTask;
  success: boolean;
}

export const useFamilyTaskAdd = () =>
  useMutation(
    (body: FamilyCalendarAddParams) => {
      return axios.post<FamilyCalendarAddResponse>('calendar', body);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('calendars');
      },
    },
  );

export const useFamilyTaskRemove = () =>
  useMutation(
    (id: number) => {
      return axios.delete<FamilyCalendarAddResponse>('calendar/' + id);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('calendars');
      },
    },
  );

const cache: any = {};

export const useFamilyTasks = (params: FamilyGetCalendarParams) =>
  useQuery(
    ['calendars', params],
    () => {
      return axios.get<FamilyGetCalendarResponse>('calendar', {
        params,
      });
    },
    {
      onSuccess: (response) => {
        response.data.data.map((task) => (cache[String(task.id)] = task));
      },
    },
  );

export const useFamilyTask = (id: number) =>
  useQuery(['calendar', id], () => cache[String(id)], {
    placeholderData: () => {
      return cache[String(id)];
    },
  });

export const useFamilyTaskAddMember = () =>
  useMutation(
    (body: FamilyAddMemberParams) => {
      return axios.post<FamilyAddMemberResponse>('calendar/addMembers', body);
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('calendars');
      },
    },
  );
