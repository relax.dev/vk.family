import { BridgePlus } from '@happysanta/bridge-plus';
import { UserGetFriendsFriend, UserInfo } from '@vkontakte/vk-bridge';
import { createContext, FC, useEffect, useState } from 'react';

export const UserContext = createContext<{
  user: UserInfo | null;
  geoData: { lat: number; long: number };
}>({
  user: null,
  geoData: { lat: 0, long: 0 },
});

export const UserProvider: FC = ({ children }) => {
  const [user, setUser] = useState<UserInfo | null>(null);
  const [geoData, setGeoData] = useState<{ lat: number; long: number }>({
    lat: 0,
    long: 0,
  });

  useEffect(() => {
    BridgePlus.getUserInfo().then((user) => {
      setUser(user);
    });

    BridgePlus.getGeodata().then((geoData) => {
      if (!geoData.available) return;

      setGeoData(geoData);
    });
  }, []);

  return (
    <UserContext.Provider value={{ user, geoData }}>
      {children}
    </UserContext.Provider>
  );
};
