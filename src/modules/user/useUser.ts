import { useContext } from "react";
import { UserContext } from "./UserProvider";

export const useUser = () => {
    const ctx = useContext(UserContext);

    return ctx;
}