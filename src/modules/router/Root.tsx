import { BridgePlus } from '@happysanta/bridge-plus';
import { useLocation } from '@happysanta/router';
import {
  ModalRoot,
  Platform,
  Root,
  SplitCol,
  SplitLayout,
  useAdaptivity,
  usePlatform,
  View,
  ViewWidth,
} from '@vkontakte/vkui';
import { count } from 'console';
import { useEffect, useState } from 'react';
import {
  MODAL_FAMILY_TASK,
  MODAL_FAMILY_TASK_ADD,
  PANEL_FAMILY_ADD,
  PANEL_FAMILY_CREATE,
  PANEL_FAMILY_EDIT,
  PANEL_FAMILY_GEO,
  PANEL_FAMILY_TASKS,
  PANEL_MAIN,
  router,
  VIEW_MAIN,
} from '.';
import { FamilyAdd } from '../../views/screens/FamilyAdd/FamilyAdd';
import { FamilyCreate } from '../../views/screens/FamilyCreate/FamilyCreate';
import { FamilyEdit } from '../../views/screens/FamilyEdit/FamilyEdit';
import { FamilyGeo } from '../../views/screens/FamilyGeo/FamilyGeo';
import { FamilyTask } from '../../views/screens/FamilyTask/FamilyTask';
import { FamilyTaskAdd } from '../../views/screens/FamilyTaskAdd/FamilyTaskAdd';
import { FamilyTasks } from '../../views/screens/FamilyTasks/FamilyTasks';
import { Home } from '../../views/screens/Home/Home';
import { useFamily, useFamilyGeoSend } from '../api';
import { openFamilyCreatePage } from './moves';

export const RootStructure = () => {
  const location = useLocation();
  const view = location.getViewId();
  const panel = location.getViewActivePanel(view) || PANEL_MAIN;
  const family = useFamily();
  console.log(family);

  useEffect(() => {
    if (!family.isFetched) {
      return;
    }

    if (!family.data?.data?.user_ids?.length) {
      openFamilyCreatePage();
    }
  });

  const adaptivity = useAdaptivity();

  if (family.isLoading) {
    return null;
  }

  const modal = (
    <ModalRoot
      activeModal={location.getModalId()}
      onClose={() => router.popPage()}>
      <FamilyTaskAdd
        id={MODAL_FAMILY_TASK_ADD}
        onClose={() => router.popPage()}
      />
      <FamilyTask id={MODAL_FAMILY_TASK} onClose={() => router.popPage()} />
    </ModalRoot>
  );

  return (
    <SplitLayout modal={modal}>
      <SplitCol animate={adaptivity.viewWidth <= ViewWidth.MOBILE}>
        <Root activeView={location.getViewId()}>
          <View
            id={VIEW_MAIN}
            activePanel={panel}
            onSwipeBack={() => router.popPage()}
            history={location.getViewHistory(VIEW_MAIN)}>
            <Home id={PANEL_MAIN} />
            <FamilyCreate id={PANEL_FAMILY_CREATE} />
            <FamilyEdit id={PANEL_FAMILY_EDIT} />
            <FamilyAdd id={PANEL_FAMILY_ADD} />
            <FamilyGeo id={PANEL_FAMILY_GEO} />
            <FamilyTasks id={PANEL_FAMILY_TASKS} />
          </View>
        </Root>
      </SplitCol>
    </SplitLayout>
  );
};
