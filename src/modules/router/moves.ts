import { PAGE_MAIN } from "@happysanta/router"
import { MODAL_FAMILY_TASK, MODAL_FAMILY_TASK_ADD, PAGE_FAMILY_ADD, PAGE_FAMILY_CREATE, PAGE_FAMILY_EDIT, PAGE_FAMILY_GEO, PAGE_FAMILY_TASKS, router } from "."

export const openFamilyEditPage = () => {
    router.pushPage(PAGE_FAMILY_EDIT)
}

export const openFamilyCreatePage = () => {
    router.pushPage(PAGE_FAMILY_CREATE)
}

export const openFamilyGeoPage = () => {
    router.pushPage(PAGE_FAMILY_GEO)
}

export const openFamilyAddPage = () => {
    router.pushPage(PAGE_FAMILY_ADD)
}

export const openMainPage = () => {
    router.pushPage(PAGE_MAIN)
}

export const openFamilyTaskAdd = () => {
    router.pushModal(MODAL_FAMILY_TASK_ADD);
}

export const openFamilyTask = (id: string) => {
    router.pushModal(MODAL_FAMILY_TASK, { taskId: id });
}

export const openFamilyTasks = () => {
    router.pushPage(PAGE_FAMILY_TASKS);
}