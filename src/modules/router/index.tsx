import { Page, Router } from '@happysanta/router';

export const VIEW_MAIN = 'view_main';

export const PANEL_MAIN = 'panel_main';
export const PANEL_FAMILY_CREATE = 'panel_family_create';
export const PANEL_FAMILY_EDIT = 'panel_family_edit';
export const PANEL_FAMILY_GEO = 'panel_family_geo';
export const PANEL_FAMILY_ADD = 'panel_family_add';
export const PANEL_FAMILY_TASKS = 'panel_family_tasks';

export const MODAL_FAMILY_TASK_ADD = 'modal_family_task_add';
export const MODAL_FAMILY_TASK = 'modal_family_task';

export const PAGE_MAIN = '/';
export const PAGE_FAMILY_CREATE = '/family/create';
export const PAGE_FAMILY_EDIT = 'family/edit';
export const PAGE_FAMILY_GEO = 'family/geo';
export const PAGE_FAMILY_ADD = 'family/add';
export const PAGE_FAMILY_TASKS = 'family/tasks';

const routes = {
  [PAGE_MAIN]: new Page(PANEL_MAIN, VIEW_MAIN),
  [PAGE_FAMILY_CREATE]: new Page(PANEL_FAMILY_CREATE, VIEW_MAIN),
  [PAGE_FAMILY_EDIT]: new Page(PANEL_FAMILY_EDIT, VIEW_MAIN),
  [PAGE_FAMILY_GEO]: new Page(PANEL_FAMILY_GEO, VIEW_MAIN),
  [PAGE_FAMILY_ADD]: new Page(PANEL_FAMILY_ADD, VIEW_MAIN),
  [PAGE_FAMILY_TASKS]: new Page(PANEL_FAMILY_TASKS, VIEW_MAIN),
};

export const router = new Router(routes);
