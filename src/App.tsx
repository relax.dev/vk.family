import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import {
  AdaptivityProvider,
  AppRoot,
  ConfigProvider,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  Root,
  View,
} from '@vkontakte/vkui';
import { useLocation, useRouter } from '@happysanta/router';
import {
  PAGE_FAMILY_CREATE,
  PANEL_FAMILY_ADD,
  PANEL_FAMILY_CREATE,
  PANEL_FAMILY_EDIT,
  PANEL_MAIN,
  router,
  VIEW_MAIN,
} from './modules/router';
import { FamilyCreate } from './views/screens/FamilyCreate/FamilyCreate';
import { UserProvider } from './modules/user/UserProvider';
import { FamilyEdit } from './views/screens/FamilyEdit/FamilyEdit';
import { FamilyAdd } from './views/screens/FamilyAdd/FamilyAdd';
import { QueryClientProvider } from 'react-query';
import { queryClient, useFamilyGeo, useFamilyGeoSend } from './modules/api';
import { RootStructure } from './modules/router/Root';
import { YMaps } from 'react-yandex-maps';
import { BridgePlus } from '@happysanta/bridge-plus';

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <UserProvider>
        <ConfigProvider>
          <AdaptivityProvider>
            <AppRoot>
              <RootStructure />
            </AppRoot>
          </AdaptivityProvider>
        </ConfigProvider>
      </UserProvider>
    </QueryClientProvider>
  );
}

export default App;
