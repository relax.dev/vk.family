import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import bridge from '@vkontakte/vk-bridge';
import { RouterContext } from '@happysanta/router';
import { router } from './modules/router/';
import '@vkontakte/vkui/dist/vkui.css';
import { BridgePlus } from '@happysanta/bridge-plus';
import { UsersCache } from './cache';

BridgePlus.init();
router.start();

export const addToCacheMany = (ids: Array<string | number>, data: any) => {
  UsersCache.getInstance().cache[ids.join('-') + '-many'] = data;
};

export const getFromCacheMany = async (ids: Array<string | number>) => {
  if (UsersCache.getInstance().cache[ids.join('-') + '-many']) {
    return UsersCache.getInstance().cache[ids.join('-') + '-many'];
  } else {
    return false;
  }
};

export const addToCache = (id: number | string, data: any) => {
  UsersCache.getInstance().cache[id.toString()] = data;
};

export const getFromCache = (id: number | string) => {
  if (UsersCache.getInstance().cache[id.toString()]) {
    console.log('CACHE', UsersCache.getInstance().cache[id.toString()]);
    return UsersCache.getInstance().cache[id.toString()];
  } else {
    return false;
  }
};

ReactDOM.render(
  <React.StrictMode>
    <RouterContext.Provider value={router}>
      <App />
    </RouterContext.Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
