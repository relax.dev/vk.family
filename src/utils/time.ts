import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru');

export const time = moment;

export const getDaysArray = () => {
    const startOfWeek = moment()
    const endOfWeek = moment().add(6, 'day')

    const days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }

    return days;
}


export const formatDate = () => {

}